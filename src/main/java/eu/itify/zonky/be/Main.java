package eu.itify.zonky.be;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import org.apache.http.client.methods.RequestBuilder;

public class Main {

	private static final long EXECUTION_PERIOD = 5 * 60 * 1000; // 5 minutes

	private static final DateFormat ISO_8601_DATE_FORMAT;

	static {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

		dateFormat.setTimeZone(TimeZone.getDefault());

		ISO_8601_DATE_FORMAT = dateFormat;
	}

	public static void main(String[] args) {
		RequestBlueprintBuilder loanRequestBlueprintBuilder = (currentTimestamp, lastTimestamp) -> {
			RequestBuilder requestBlueprint = RequestBuilder.get("https://api.zonky.cz/loans/marketplace");

			// exclude loans published after the current iteration started
			requestBlueprint.addParameter("datePublished__lt", ISO_8601_DATE_FORMAT.format(currentTimestamp));
			// exclude loans we processed in previous iterations
			if (lastTimestamp != null)
				requestBlueprint.addParameter("datePublished__gte", ISO_8601_DATE_FORMAT.format(lastTimestamp));

			// make sure the returned data is sorted so that the paging employed
			// in ZonkyQueryTask is well defined
			requestBlueprint.addHeader("X-Order", "datePublished");

			return requestBlueprint;
		};
		// a simple processor which logs loan data to the standard output in a human readable form
		JsonValueBatchProcessor loanLogger = new JsonValueBatchProcessor() {

			private PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8));

			private int index;

			@Override
			public void startBatch(Date timestamp) {
				writer.println("--- " + ISO_8601_DATE_FORMAT.format(timestamp) + " ---------------------------------");
				writer.println();
				writer.flush();
				index = 0;
			}

			@Override
			public void process(JsonNode data) {
				writer.println("#     : " + ++index);
				writer.println("Id    : " + data.get("id").asLong());
				writer.println("User  : " + data.get("nickName").asText());
				writer.println("Name  : " + data.get("name").asText());
				writer.println("Amount: " + data.get("amount").asDouble());
				writer.println("Date  : " + data.get("datePublished").asText());
				writer.println();
				writer.flush();
			}

		};
		Timer timer = new Timer("loan-query-timer");

		// schedule the task every EXECUTION_PERIOD
		timer.schedule(
			new ZonkyQueryTask(loanRequestBlueprintBuilder, loanLogger),
			0, EXECUTION_PERIOD
		);
	}

}
