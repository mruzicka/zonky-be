package eu.itify.zonky.be;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.TimerTask;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * A {@link TimerTask} specialized for executing HTTP requests based on blueprints created by
 * a {@link RequestBlueprintBuilder} and processing the JSON data returned in the respective
 * HTTP responses by delegating to a {@link JsonValueBatchProcessor}.
 */
@SuppressWarnings("WeakerAccess")
public class ZonkyQueryTask extends TimerTask {

	protected static final String HEADER_PAGE_NUMBER = "X-Page";

	protected static final String HEADER_PAGE_SIZE = "X-Size";

	protected static final String HEADER_TOTAL_COUNT = "X-Total";

	protected static final long PAGE_SIZE = 1000;

	/**
	 * The request blueprint builder to use for creating the blueprints of the HTTP
	 * requests executed by this task
	 */
	protected final RequestBlueprintBuilder requestBlueprintBuilder;
	/**
	 * The JSON value batch processor to use for delegation of processing of the elements
	 * of the JSON arrays returned form the HTTP requests executed by this task
	 */
	protected final JsonValueBatchProcessor jsonValueBatchProcessor;

	protected final CloseableHttpClient httpClient = HttpClients.createDefault();
	protected final JsonFactory jsonFactory = new JsonFactory();

	protected Date lastRunTimestamp;

	/**
	 * Creates an instance with the specified {@code requestBlueprintBuilder} and
	 * {@code jsonValueBatchProcessor}.
	 *
	 * @param requestBlueprintBuilder a request blueprint builder to use for creating the blueprints
	 *                                of the HTTP requests executed by this task
	 * @param jsonValueBatchProcessor a JSON value batch processor to use for delegation of processing
	 *                                of the elements of the JSON arrays returned form the HTTP requests
	 *                                executed by this task
	 */
	public ZonkyQueryTask(RequestBlueprintBuilder requestBlueprintBuilder, JsonValueBatchProcessor jsonValueBatchProcessor) {
		this.requestBlueprintBuilder = requestBlueprintBuilder;
		this.jsonValueBatchProcessor = jsonValueBatchProcessor;
		jsonFactory.setCodec(new ObjectMapper());
	}

	protected Charset getCharset(HttpEntity entity) {
		ContentType contentType = ContentType.get(entity);

		if (contentType != null) {
			Charset charset = contentType.getCharset();

			if (charset != null)
				return charset;

		}

		return StandardCharsets.UTF_8;
	}

	protected int getTotalCount(CloseableHttpResponse response) {
		Header header = response.getFirstHeader(HEADER_TOTAL_COUNT);

		if (header != null) {
			try {
				return Integer.parseInt(header.getValue());
			} catch (NumberFormatException ignore) {
			}
		}

		return 0;
	}

	/**
	 * Process the specified HTTP response assuming its content is a JSON array.
	 * Delegate the processing of each element of the array to the
	 * {@link #jsonValueBatchProcessor}
	 *
	 * @param response the HTTP response to process
	 * @return the number of elements of the JSON array in the response content
	 */
	protected int processResponse(HttpResponse response) throws IOException {
		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
			throw new RuntimeException("Unexpected HTTP status: " + response.getStatusLine());

		HttpEntity entity = response.getEntity();
		Reader contentReader = new InputStreamReader(entity.getContent(), getCharset(entity));
		JsonParser responseParser = jsonFactory.createParser(contentReader);

		responseParser.nextToken();
		if (!responseParser.isExpectedStartArrayToken())
			throw new RuntimeException("Response is not a JSON array");

		int count = 0;

		while (responseParser.nextToken() != JsonToken.END_ARRAY) {
			JsonNode value = responseParser.readValueAsTree();

			jsonValueBatchProcessor.process(value);
			++count;
		}

		return count;
	}

	/**
	 * Execute a HTTP request based on the specified request blueprint and process
	 * the response.
	 * <p>
	 * Note that if the resulting data set is larger than the {@link #PAGE_SIZE}
	 * then additional requests based on the blueprint are executed with appropriate
	 * paging options and the corresponding responses are processed until the entire
	 * resulting data set is processed.
	 *
	 * @param requestBlueprint the blueprint of the HTTP request to execute
	 */
	protected void executeRequest(RequestBuilder requestBlueprint) {
		HttpUriRequest request =
			requestBlueprint
				.setHeader(HEADER_PAGE_SIZE, String.valueOf(PAGE_SIZE))
				.build();

		for (int page = 0; ; ++page) {
			request.setHeader(HEADER_PAGE_NUMBER, String.valueOf(page));

			try (
				CloseableHttpResponse response = httpClient.execute(request);
			) {
				int responseCount = processResponse(response);
				int totalCount = getTotalCount(response);

				if (page * PAGE_SIZE + responseCount >= totalCount)
					break;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public void run() {
		Date currentRunTimestamp = new Date();

		jsonValueBatchProcessor.startBatch(currentRunTimestamp);

		executeRequest(requestBlueprintBuilder.createRequestBlueprint(currentRunTimestamp, lastRunTimestamp));

		lastRunTimestamp = currentRunTimestamp;
	}

}
