package eu.itify.zonky.be;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Date;

/**
 * Implementations of this interface are used in {@link ZonkyQueryTask}
 * to process elements of the JSON arrays returned from the HTTP requests
 * executed by that task.
 */
public interface JsonValueBatchProcessor {

	/**
	 * Called before a new batch of data is produced.
	 *
	 * @param timestamp the timestamp of the creation of the batch
	 */
	void startBatch(Date timestamp);

	/**
	 * Called to process a JSON value in the form of a {@link JsonNode} tree.
	 *
	 * @param value the JSON value to process
	 */
	void process(JsonNode value);

}
