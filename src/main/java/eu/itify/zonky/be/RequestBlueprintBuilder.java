package eu.itify.zonky.be;

import java.util.Date;
import org.apache.http.client.methods.RequestBuilder;

/**
 * Implementations of this interface are used by {@link ZonkyQueryTask} to create
 * request blueprints for HTTP requests executed by that task.
 */
public interface RequestBlueprintBuilder {

	/**
	 * Return a blueprint for HTTP requests given the current timestamp and the timestamp
	 * of the last creation of the blueprint.
	 *
	 * @param currentTimestamp the current timestamp
	 * @param lastTimestamp    the timestamp of the last creation of the blueprint
	 * @return the blueprint for the HTTP requests
	 */
	RequestBuilder createRequestBlueprint(Date currentTimestamp, Date lastTimestamp);

}
